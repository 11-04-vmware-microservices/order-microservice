package com.classpath.ordermicroservice.channel;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface OutputChannel {

    @Output("orders")
    MessageChannel ordersTopic();

    @Output("inventory")
    MessageChannel inventoryTopic();
}
