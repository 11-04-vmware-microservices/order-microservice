package com.classpath.ordermicroservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.*;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.LinkedHashMap;
import java.util.Map;
import static org.springframework.boot.availability.LivenessState.BROKEN;
import static org.springframework.boot.availability.LivenessState.CORRECT;
import static org.springframework.boot.availability.ReadinessState.ACCEPTING_TRAFFIC;
import static org.springframework.boot.availability.ReadinessState.REFUSING_TRAFFIC;

@RestController
@RequestMapping("/state/")
@RequiredArgsConstructor
@Slf4j
public class ProbedController {
    private final ApplicationAvailability applicationAvailability;
    private final ApplicationEventPublisher eventPublisher;

    @PostMapping("/liveness")
    public Map<String, Object> updateLivessnessState(){
        LivenessState livenessState = this.applicationAvailability.getLivenessState();
        log.info("Current liveness state :: {} ", livenessState );
        LivenessState updatedLivenessState = livenessState == CORRECT ? BROKEN : CORRECT;
        Map<String, Object> resultMap = new LinkedHashMap<>();
        String state =  updatedLivenessState == CORRECT ? "UP" : "DOWN";
        resultMap.put("state", updatedLivenessState);
        resultMap.put("status", state);
        AvailabilityChangeEvent.publish(eventPublisher, state , updatedLivenessState);
        return resultMap;
    }

    @PostMapping("/readiness")
    public Map<String, Object> updateReadinessState(){
        ReadinessState readiness = this.applicationAvailability.getReadinessState();
        log.info("Current readiness state :: {} ", readiness );
        ReadinessState updatedReadinessState = readiness == ACCEPTING_TRAFFIC ? REFUSING_TRAFFIC : ACCEPTING_TRAFFIC;
        Map<String, Object> resultMap = new LinkedHashMap<>();
        String state =  updatedReadinessState == ACCEPTING_TRAFFIC ? "UP" : "DOWN";
        resultMap.put("state", updatedReadinessState);
        resultMap.put("status", state);
        AvailabilityChangeEvent.publish(eventPublisher, state , updatedReadinessState);
        return resultMap;
    }
}
