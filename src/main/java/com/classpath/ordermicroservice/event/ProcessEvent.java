package com.classpath.ordermicroservice.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.AvailabilityState;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@Slf4j
public class ProcessEvent {

    @EventListener
    public void processLivenessEvent(AvailabilityChangeEvent<LivenessState> changeEvent){
        log.info("*****************************");
        log.info("Updated event on Liveness :: {} ", changeEvent.getState());
        log.info("*****************************");
    }

    @EventListener
    public void processReadinessEvent(AvailabilityChangeEvent<ReadinessState> changeEvent){
        log.info("*****************************");
        log.info("Updated event on Readiness :: {} ", changeEvent.getState());
        log.info("*****************************");
    }
}
