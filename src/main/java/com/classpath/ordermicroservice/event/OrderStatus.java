package com.classpath.ordermicroservice.event;

public enum OrderStatus {
    ORDER_ACCEPTED,
    ORDER_REJECTED,
    ORDER_FULFILLED
}
