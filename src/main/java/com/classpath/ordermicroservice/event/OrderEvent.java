package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Builder
public class OrderEvent {
    private Order order;
    private OrderStatus orderStatus;
}
