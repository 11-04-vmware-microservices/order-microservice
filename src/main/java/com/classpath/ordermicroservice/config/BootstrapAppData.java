package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import java.util.stream.IntStream;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class BootstrapAppData {

    private final OrderRepository orderRepository;

    @Value("${app.ordercount}")
    private int orderCount;
    private final Faker faker = new Faker();

    @EventListener
    public void onApplicationReady(ApplicationReadyEvent readyEvent){
        IntStream.range(0, 101)
                .forEach(index -> {
                    //Order order = new Order(1, 34, true, true, false, "Vinod" ,"Hemant")
                    Order order = Order.builder()
                                            .name(faker.name().fullName())
                                            .email(faker.internet().emailAddress())
                                        .build();
                    IntStream.range(1, 3).forEach(value -> {
                        LineItem lineItem = LineItem
                                                .builder()
                                                    .qty(faker.number().numberBetween(2, 5))
                                                    .price(faker.number().randomDouble(2, 500, 800))
                                                    .name(faker.commerce().productName())
                                                    .build();
                        order.setPrice(order.getPrice() + lineItem.getQty() * lineItem.getPrice());
                        order.addLineItem(lineItem);
                    });
                    this.orderRepository.save(order);
                });
    }
}
