package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.channel.OutputChannel;
import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.event.OrderStatus;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    //private static final Logger logger = Logger.getLogger(OrderService.class)
    private final OrderRepository orderRepository;

    private final WebClient webClient;
    private final OutputChannel source;

    public Set<Order> fetchOrders(){
        log.info("Fetching all the orders");
        return new HashSet<>(this.orderRepository.findAll());
        //return Set.copyOf(this.orderRepository.findAll());
    }

    public Order fetchOrderById(long orderId){
        log.info("Fetching the order by id :: {}", orderId);
        return this.orderRepository
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
    }

    public void deleteById(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    @Transactional
    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallback")
    public Order saveOrder(Order order){
        Order savedOrder = this.orderRepository.save(order);

       /* Integer response = this.webClient
                                .post()
                                .uri("/api/inventory")
                                .retrieve()
                                .bodyToMono(Integer.class)
                                .block();
        log.info("Response from save order function ::  {}", response);*/

        OrderEvent orderAccepted = OrderEvent
                                        .builder()
                                            .order(savedOrder)
                                            .orderStatus(OrderStatus.ORDER_ACCEPTED)
                                        .build();
        this.source.ordersTopic().send(MessageBuilder.withPayload(orderAccepted).build());
        return savedOrder;
    }

    private Order fallback(Throwable throwable){
        log.error("Exception while updating the inventory:: {}", throwable.getMessage());
        return Order.builder().build();
    }


}
