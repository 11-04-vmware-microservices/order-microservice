package com.classpath.ordermicroservice.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String name;
    private String email;
    private double price;
    @OneToMany(mappedBy = "order", cascade = ALL, fetch = EAGER)
    @JsonManagedReference
    private Set<LineItem> lineItems;

    public void addLineItem(LineItem lineItem){
        if (this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}
