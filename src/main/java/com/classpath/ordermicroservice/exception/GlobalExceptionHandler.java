package com.classpath.ordermicroservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error handleInvalidOrderId(IllegalArgumentException exception){
      log.error("Exception with invalid order id :: {}", exception.getMessage());
      return new Error(100, exception.getMessage());
    }
}

@AllArgsConstructor
@Getter
class Error{
    private int code;
    private String message;
}
